#! /usr/bin/env python

import rospy
from debug_case2.srv import GetPose
from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry

class GetPoseServer():

    def __init__(self):
        self.my_service = rospy.Service('/get_pose_service', GetPose , self.service_callback)
        self.sub_pose = rospy.Subscriber('/odometry/filtered', Odometry, self.sub_callback)
        self.robot_pose = Pose()

    def service_callback(self, request):
        response = GetPose()
        response = self.robot_pose
        return response

    def sub_callback(self, msg):
        self.robot_pose = msg

if __name__ == '__main__':
    rospy.init_node('get_pose_server_node', anonymous=True)
    getposeserver_object = GetPoseServer()