#! /usr/bin/env python

import rospy
from debug_case2.srv import GetPose

class GetPoseClient():

    def __init__(self):
        rospy.wait_for_service('/get_pose_service')
        self.get_pose_client = rospy.ServiceProxy('/get_pose_service', GetPose)

    def call_server(self):
        get_pose_request = GetPose()
        result = self.get_pose_client(get_pose_request)
        print result

if __name__ == '__main__':
    rospy.init_node('get_pose_client_node')
    getposeclient_object = GetPoseClient()
    getposeclient_object.call_server()